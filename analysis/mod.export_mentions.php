<?php
require_once __DIR__ . '/common/config.php';
//require_once __DIR__ . '/common/functions.php';
require_once __DIR__ . '/common/CSV.class.php';
require_once __DIR__ . '/common/pseudonymization.php';

validate_all_variables();
dataset_must_exist();
$dbh = pdo_connect();
pdo_unbuffered($dbh);

$filename = get_filename_for_export('mentionExport');
$stream_to_open = export_start($filename, $outputformat);

$csv = new CSV($stream_to_open, $outputformat);

$csv->writeheader(array('tweet_id', 'user_from_id', 'user_from_name', 'user_to_id', 'user_to_name', 'mention_type'));

$sql = "SELECT t.id as id, t.text as text, m.from_user_id as user_from_id, m.from_user_name as user_from_name, m.to_user_id as user_to_id, m.to_user as user_to_name FROM " . $esc['mysql']['dataset'] . "_mentions m, " . $esc['mysql']['dataset'] . "_tweets t ";
$sql .= sqlSubset();
$sql .= " AND m.tweet_id = t.id ORDER BY id";

$out = "";

$rec = $dbh->prepare($sql);
$rec->execute();

// Create a boolean variable that gives whether a dataset is marked for pseudonymization or not.
$pseudonymized_bool = is_pseudonymized($esc['mysql']['dataset']);

// Fetch the pseudonymization list from the database and save it into the array $pseudo_list2
$pseudo_list2 = array();
$pseudo_list2 = fetch_pseudonymized_data();

// If there exists a pseudonymization list we need the index value from that list since this value will be used as a reference number.
if ($pseudo_list2 != NULL) {
    $start_index = $last_index = max(array_keys($pseudo_list2));
    $pp = $pseudo_list2;
} else { // If no pseudonymization lists exist, start from 0.
    $start_index = $last_index = 0;
    $pp = array();
}

// The array that contains all the fields in a tweet object that we want to pseudonymize. Created as assocational array for the sake of speed.
$keyarray = array('location' => 1, 'username' => 1, 'user' => 2, 'id' => 3, 'tweetid' => 4, 'id_string' => 1, 'from_user_id' => 1, 'from_user_name' => 1, 'from_user_realname' => 1, 'user_from_name' => 1, 'user_from_id' => 1, 'user_to_id' => 1, 'user_to_name' => 1, 'to_user' => 1, 'to_user_id' => 1, 'to_user_name' => 1, 'in_reply_to_status_id' => 1, 'in_reply_to_status_id_str' => 1, 'in_reply_to_user_id' => 1, 'in_reply_to_screen_name' => 1, 'quoted_status_id' => 1, 'retweeted_status' => 1, 'retweeted' => 1, 'retweet_id' => 1);

while ($data = $rec->fetch(PDO::FETCH_ASSOC)) {
    // Use that boolean value to determine whether we should send the fetched dataparts to the function pseudonymized.
    if ($pseudonymized_bool == 1) {
        // Send row-by-row to the pseudonymization functionality together with our index value.
        $last_index  = pseudonymize($data, $pp);
    }

    $csv->newrow();
    $csv->addfield($data['id'], 'integer');
    $csv->addfield($data['user_from_id'], 'integer');
    $csv->addfield($data['user_from_name'], 'string');
    $csv->addfield($data['user_to_id'], 'integer');
    $csv->addfield($data['user_to_name'], 'string');
    $csv->addfield(detect_mention_type($data['text'], $data['user_to_name']), 'string');
    $csv->writerow();
}

$csv->close();

// If the current collection is marked for pseudonymization then all the values should be saved after the CSV-loop where we have run our pseudonymization functionality.
if ($pseudonymized_bool == 1) {
    save_pseudonymized_data($pp, $start_index, $GLOBALS['last_index']);
}

if (!$use_cache_file) {
    exit(0);
}
// Rest of script is the HTML page with a link to the cached CSV/TSV file.
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>TCAT :: Export mentions</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="css/main.css" type="text/css" />

    <script type="text/javascript" language="javascript">



    </script>

</head>

<body>

    <h1>TCAT :: Export mentions</h1>

    <?php
    echo '<fieldset class="if_parameters">';
    echo '<legend>Your File</legend>';
    echo '<p><a href="' . filename_to_url($filename) . '">' . $filename . '</a></p>';
    echo '</fieldset>';
    ?>

</body>

</html>