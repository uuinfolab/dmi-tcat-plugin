<?php
require_once __DIR__ . '/common/config.php';
require_once __DIR__ . '/common/functions.php';
require_once __DIR__ . '/common/Gexf.class.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>TCAT :: Source / hashtag co-occurence</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="css/main.css" type="text/css" />

    <script type="text/javascript" language="javascript">



    </script>

</head>

<body>

    <h1>TCAT :: Source / hashtag co-occurence</h1>

    <?php

    validate_all_variables();
    dataset_must_exist();
    $dbh = pdo_connect();
    pdo_unbuffered($dbh);
    $filename = get_filename_for_export("sourceHashtag", '', 'gexf');
    $collation = current_collation();

    //print_r($_GET);

    $sql = "SELECT LOWER(t.source COLLATE $collation) AS source, LOWER(h.text COLLATE $collation) AS hashtag FROM ";
    $sql .= $esc['mysql']['dataset'] . "_hashtags h, " . $esc['mysql']['dataset'] . "_tweets t ";
    $where = "t.id = h.tweet_id AND ";
    $sql .= sqlSubset($where);

    $rec = $dbh->prepare($sql);
    $rec->execute();

    // Create a boolean variable that gives whether a dataset is marked for pseudonymization or not.
    $pseudonymized_bool = is_pseudonymized($esc['mysql']['dataset']);

    // Fetch the pseudonymization list from the database and save it into the array $pseudo_list2
    $pseudo_list2 = array();
    $pseudo_list2 = fetch_pseudonymized_data();

    // If there exists a pseudonymization list we need the index value from that list since this value will be used as a reference number.
    if ($pseudo_list2 != NULL) {
        $start_index = $last_index = max(array_keys($pseudo_list2));
        $pp = $pseudo_list2;
    } else { // If no pseudonymization lists exist, start from 0.
        $start_index = $last_index = 0;
        $pp = array();
    }

    // The array that contains all the fields in a tweet object that we want to pseudonymize. Created as assocational array for the sake of speed.
    $keyarray = array('location' => 1, 'username' => 1, 'user' => 2, 'id' => 3, 'tweetid' => 4, 'id_string' => 1, 'from_user_id' => 1, 'from_user_name' => 1, 'from_user_realname' => 1, 'user_from_name' => 1, 'user_from_id' => 1, 'user_to_id' => 1, 'user_to_name' => 1, 'to_user' => 1, 'to_user_id' => 1, 'to_user_name' => 1, 'in_reply_to_status_id' => 1, 'in_reply_to_status_id_str' => 1, 'in_reply_to_user_id' => 1, 'in_reply_to_screen_name' => 1, 'quoted_status_id' => 1, 'retweeted_status' => 1, 'retweeted' => 1, 'retweet_id' => 1);

    while ($res = $rec->fetch(PDO::FETCH_ASSOC)) {
        // Use that boolean value to determine whether we should send the fetched dataparts to the function pseudonymized.
        if ($pseudonymized_bool == 1) {
            // Send row-by-row to the pseudonymization functionality together with our index value.
            $last_index  = pseudonymize($res, $pp);
        }

        $res['source'] = preg_replace("/<.+>/U", "", $res['source']);
        $res['source'] = preg_replace("/[ \s\t]+/", " ", $res['source']);
        $res['source'] = trim($res['source']);

        if (!isset($sourcesHashtags[$res['source']][$res['hashtag']])) {
            $sourcesHashtags[$res['source']][$res['hashtag']] = 0;
        }
        $sourcesHashtags[$res['source']][$res['hashtag']]++;
    }

    $gexf = new Gexf();
    $gexf->setTitle("source-hashtag " . $filename);
    $gexf->setEdgeType(GEXF_EDGE_UNDIRECTED);
    $gexf->setCreator("tools.digitalmethods.net");
    foreach ($sourcesHashtags as $source => $hashtags) {
        foreach ($hashtags as $hashtag => $frequency) {
            $node1 = new GexfNode($source);
            $node1->addNodeAttribute("type", 'source', $type = "string");
            $gexf->addNode($node1);
            $node2 = new GexfNode($hashtag);
            $node2->addNodeAttribute("type", 'hashtag', $type = "string");
            $gexf->addNode($node2);
            $edge_id = $gexf->addEdge($node1, $node2, $frequency);
        }
    }

    $gexf->render();

    file_put_contents($filename, $gexf->gexfFile);

    echo '<fieldset class="if_parameters">';

    echo '<legend>Your network (GEXF) file</legend>';

    echo '<p><a href="' . filename_to_url($filename) . '">' . $filename . '</a></p>';

    echo '</fieldset>';

    // If the current collection is marked for pseudonymization then all the values should be saved after the CSV-loop where we have run our pseudonymization functionality.
    if ($pseudonymized_bool == 1) {
        save_pseudonymized_data($pp, $start_index, $GLOBALS['last_index']);
    }
    ?>

</body>

</html>