<?php
require_once __DIR__ . '/common/config.php';
require_once __DIR__ . '/common/CSV.class.php';
require_once __DIR__ . '/common/pseudonymization.php';
/*
* mod.export_pseudonymization.php is the file responsible for exporting the pseudonymization table.
*/
//Check if the current user is admin
if (!(is_admin())) {
    echo ("You need to be admin to export the pseudonymization table");
}

// Open database connection and open a stream to be able to create a .CSV file.
$dbh = pdo_connect();
pdo_unbuffered($dbh);
$stream_to_open  =  export_start("Pseudonymization table", 'csv');

// Create a new CSV-object.
$csv = new CSV($stream_to_open, 'csv');

//Construct our header.
$csv->writeheader(array('pseudo_val', 'original_data', 'fieldtype'));

// Fetch all data from the table
$sql = "SELECT * FROM tcat_pseudonymized_data;";
$rec = $dbh->prepare($sql);
$rec->execute();

// take each field from the pseudonymization table and add each field.
while ($data = $rec->fetch(PDO::FETCH_ASSOC)) {

    // Match eatch field in the fetched row to its corresponding CSV-column.
    $csv->newrow();
    $csv->addfield($data['pseudo_val'], 'integer');
    $csv->addfield($data['original_data'], 'string');
    $csv->addfield($data['fieldtype'], 'string');
    $csv->writerow();
}
$csv->close();
