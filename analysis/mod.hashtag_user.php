<?php
require_once __DIR__ . '/common/config.php';
require_once __DIR__ . '/common/functions.php';
require_once __DIR__ . '/common/Gexf.class.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>TCAT :: Bipartite hashtag - user graph</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="css/main.css" type="text/css" />

</head>

<body>

    <h1>TCAT :: Bipartite hashtag - user graph</h1>

    <?php
    validate_all_variables();
    dataset_must_exist();
    $dbh = pdo_connect();
    pdo_unbuffered($dbh);

    $filename = get_filename_for_export("hashtagUser", (isset($_GET['probabilityOfAssociation']) ? "_normalizedAssociationWeight" : ""), "gexf");

    include_once __DIR__ . '/common/Coword.class.php';
    $coword = new Coword;
    $coword->countWordOncePerDocument = FALSE;

    $collation = current_collation();

    // get hashtag-user relations
    $sql = "SELECT LOWER(A.text COLLATE $collation) AS h1, LOWER(A.from_user_name COLLATE $collation) AS user, LOWER(t.from_user_lang) AS language, LOWER(t.location COLLATE $collation) AS location, t.from_user_timezone AS timezone, t.from_user_utcoffset AS utcoffset ";
    $sql .= "FROM " . $esc['mysql']['dataset'] . "_hashtags A, " . $esc['mysql']['dataset'] . "_tweets t ";
    $sql .= sqlSubset() . " AND ";
    $sql .= "LENGTH(A.text)>1 AND ";
    $sql .= "A.tweet_id = t.id ";

    $languages = $locations = array();
    $rec = $dbh->prepare($sql);
    $rec->execute();

    // Create a boolean variable that gives whether a dataset is marked for pseudonymization or not.
    $pseudonymized_bool = is_pseudonymized($esc['mysql']['dataset']);

    // Fetch the pseudonymization list from the database and save it into the array $pseudo_list2
    $pseudo_list2 = array();
    $pseudo_list2 = fetch_pseudonymized_data();

    // If there exists a pseudonymization list we need the index value from that list since this value will be used as a reference number.
    if ($pseudo_list2 != NULL) {
        $start_index = $last_index = max(array_keys($pseudo_list2));
        $pp = $pseudo_list2;
    } else { // If no pseudonymization lists exist, start from 0.
        $start_index = $last_index = 0;
        $pp = array();
    }

    // The array that contains all the fields in a tweet object that we want to pseudonymize. Created as assocational array for the sake of speed.
    $keyarray = array('location' => 1, 'username' => 1, 'user' => 2, 'id' => 3, 'tweetid' => 4, 'id_string' => 1, 'from_user_id' => 1, 'from_user_name' => 1, 'from_user_realname' => 1, 'user_from_name' => 1, 'user_from_id' => 1, 'user_to_id' => 1, 'user_to_name' => 1, 'to_user' => 1, 'to_user_id' => 1, 'to_user_name' => 1, 'in_reply_to_status_id' => 1, 'in_reply_to_status_id_str' => 1, 'in_reply_to_user_id' => 1, 'in_reply_to_screen_name' => 1, 'quoted_status_id' => 1, 'retweeted_status' => 1, 'retweeted' => 1, 'retweet_id' => 1);

    while ($res = $rec->fetch(PDO::FETCH_ASSOC)) {
        // Use that boolean value to determine whether we should send the fetched dataparts to the function pseudonymized.
        if ($pseudonymized_bool == 1) {
            // Send row-by-row to the pseudonymization functionality together with our index value.
            $last_index  = pseudonymize($res, $pp);
        }

        if (!isset($userHashtags[$res['user']][$res['h1']]))
            $userHashtags[$res['user']][$res['h1']] = 0;
        $userHashtags[$res['user']][$res['h1']]++;
        if (!isset($userCount[$res['user']]))
            $userCount[$res['user']] = 0;
        $userCount[$res['user']]++;
        if (!isset($hashtagCount[$res['h1']]))
            $hashtagCount[$res['h1']] = 0;
        $hashtagCount[$res['h1']]++;
        $languages[$res['user']] = $res['language'];
        $locations[$res['user']] = $res['location'];
        $from_user_timezone[$res['user']] = $res['timezone'];
        $from_user_utcoffset[$res['user']] = $res['utcoffset'];
    }

    $gexf = new Gexf();
    $gexf->setTitle("Hashtag - user " . $filename);
    $gexf->setEdgeType(GEXF_EDGE_UNDIRECTED);
    $gexf->setCreator("tools.digitalmethods.net");

    foreach ($userHashtags as $user => $hashtags) {
        foreach ($hashtags as $hashtag => $frequency) {
            $node1 = new GexfNode($user);
            $node1->id = md5('n-user_' . $user);
            $node1->addNodeAttribute("type", 'user', $type = "string");
            $node1->addNodeAttribute("userFrequency", $userCount[$user], $type = "int");
            $node1->addNodeAttribute("hashtagFrequency", 0, $type = "int");
            $node1->addNodeAttribute("language", $languages[$user], $type = "string");
            $node1->addNodeAttribute("location", $locations[$user], $type = "string");
            $node1->addNodeAttribute("from_user_utcoffset", $from_user_utcoffset[$user], $type = "string");
            $node1->addNodeAttribute("from_user_timezone", $from_user_timezone[$user], $type = "string");
            $gexf->addNode($node1);
            $node2 = new GexfNode($hashtag);
            $node2->id = md5('n-hashtag_' . $hashtag);
            $node2->addNodeAttribute("type", 'hashtag', $type = "string");
            $node2->addNodeAttribute("userFrequency", 0, $type = "int");
            $node2->addNodeAttribute("hashtagFrequency", $hashtagCount[$hashtag], $type = "int");
            $gexf->addNode($node2);
            $edge_id = $gexf->addEdge($node1, $node2, $frequency);
        }
    }

    $gexf->render();

    file_put_contents($filename, $gexf->gexfFile);

    // If the current collection is marked for pseudonymization then all the values should be saved after the CSV-loop where we have run our pseudonymization functionality.
    if ($pseudonymized_bool == 1) {
        save_pseudonymized_data($pp, $start_index, $GLOBALS['last_index']);
    }

    echo '<fieldset class="if_parameters">';

    echo '<legend>Your GEXF File</legend>';

    echo '<p><a href="' . filename_to_url($filename) . '">' . $filename . '</a></p>';

    echo '</fieldset>';
    ?>

</body>

</html>