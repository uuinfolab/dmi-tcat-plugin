<?php
require_once __DIR__ . '/common/config.php';
require_once __DIR__ . '/common/functions.php';
require_once __DIR__ . '/common/Gexf.class.php';
require_once __DIR__ . '/common/CSV.class.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>TCAT :: URL user co-occurence</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="css/main.css" type="text/css" />

    <script type="text/javascript" language="javascript">



    </script>

</head>

<body>

    <h1>TCAT :: URL user co-occurence</h1>

    <?php
    validate_all_variables();
    dataset_must_exist();
    $dbh = pdo_connect();
    pdo_unbuffered($dbh);
    $collation = current_collation();
    $filename = get_filename_for_export("urlUser");
    $csv = new CSV($filename, $outputformat);

    $sql = "SELECT COUNT(LOWER(t.from_user_name COLLATE $collation)) AS frequency, LOWER(t.from_user_name COLLATE $collation) AS username, u.url_followed AS url, u.domain AS domain, u.error_code AS status_code FROM ";
    $sql .= $esc['mysql']['dataset'] . "_urls u, " . $esc['mysql']['dataset'] . "_tweets t ";
    $where = "t.id = u.tweet_id AND u.url_followed !='' AND ";
    $sql .= sqlSubset($where);
    $sql .= " GROUP BY u.url_followed, LOWER(t.from_user_name) ORDER BY frequency DESC";
    $csv->writeheader(array("frequency", "user", "url", "domain", "status_code"));
    $rec = $dbh->prepare($sql);
    $rec->execute();

    // Create a boolean variable that gives whether a dataset is marked for pseudonymization or not.
    $pseudonymized_bool = is_pseudonymized($esc['mysql']['dataset']);

    // Fetch the pseudonymization list from the database and save it into the array $pseudo_list2
    $pseudo_list2 = array();
    $pseudo_list2 = fetch_pseudonymized_data();

    // If there exists a pseudonymization list we need the index value from that list since this value will be used as a reference number.
    if ($pseudo_list2 != NULL) {
        $start_index = $last_index = max(array_keys($pseudo_list2));
        $pp = $pseudo_list2;
    } else { // If no pseudonymization lists exist, start from 0.
        $start_index = $last_index = 0;
        $pp = array();
    }

    // The array that contains all the fields in a tweet object that we want to pseudonymize. Created as assocational array for the sake of speed.
    $keyarray = array('location' => 1, 'username' => 1, 'user' => 2, 'id' => 3, 'tweetid' => 4, 'id_string' => 1, 'from_user_id' => 1, 'from_user_name' => 1, 'from_user_realname' => 1, 'user_from_name' => 1, 'user_from_id' => 1, 'user_to_id' => 1, 'user_to_name' => 1, 'to_user' => 1, 'to_user_id' => 1, 'to_user_name' => 1, 'in_reply_to_status_id' => 1, 'in_reply_to_status_id_str' => 1, 'in_reply_to_user_id' => 1, 'in_reply_to_screen_name' => 1, 'quoted_status_id' => 1, 'retweeted_status' => 1, 'retweeted' => 1, 'retweet_id' => 1);


    while ($res = $rec->fetch(PDO::FETCH_ASSOC)) {

        // Use that boolean value to determine whether we should send the fetched dataparts to the function pseudonymized.
        if ($pseudonymized_bool == 1) {
            // Send row-by-row to the pseudonymization functionality together with our index value.
            $last_index  = pseudonymize($res, $pp);
        }


        $csv->newrow();
        $csv->addfield($res['frequency']);
        $csv->addfield($res['username']);
        $csv->addfield($res['url']);
        $csv->addfield($res['domain']);
        $csv->addfield($res['status_code']);
        $csv->writerow();
        $urlUsernames[$res['url']][$res['username']] = $res['frequency'];
        $urlDomain[$res['url']] = $res['domain'];
        $urlStatusCode[$res['url']] = $res['status_code'];
    }
    $csv->close();

    echo '<fieldset class="if_parameters">';

    echo '<legend>Your spreadsheet (CSV) file</legend>';

    echo '<p><a href="' . str_replace("#", urlencode("#"), str_replace("\"", "%22", $filename)) . '">' . $filename . '</a></p>';

    echo '</fieldset>';

    $userUniqueUrls = array();
    $userTotalUrls = array();
    $urlUniqueUsers = array();
    $urlTotalUsers = array();

    foreach ($urlUsernames as $url => $usernames) {
        if (!isset($urlUniqueUsers[$url])) $urlUniqueUsers[$url] = 0;
        if (!isset($urlTotalUsers[$url])) $urlTotalUsers[$url] = 0;
        foreach ($usernames as $username => $frequency) {
            if (!isset($userUniqueUrls[$username])) $userUniqueUrls[$username] = 0;
            if (!isset($userTotalUrls[$username])) $userTotalUrls[$username] = 0;
            $urlUniqueUsers[$url]++;
            $urlTotalUsers[$url] += $frequency;
            $userUniqueUrls[$username]++;
            $userTotalUrls[$username] += $frequency;
        }
    }

    $gexf = new Gexf();
    $gexf->setTitle("URL-user " . $filename);
    $gexf->setEdgeType(GEXF_EDGE_UNDIRECTED);
    $gexf->setCreator("tools.digitalmethods.net");
    foreach ($urlUsernames as $url => $usernames) {
        foreach ($usernames as $username => $frequency) {
            $node1 = new GexfNode($url);
            $node1->addNodeAttribute("type", 'url', $type = "string");
            $node1->addNodeAttribute('shortlabel', $urlDomain[$url], $type = "string");
            $node1->addNodeAttribute('longlabel', $url, $type = "string");
            $node1->addNodeAttribute('status_code', $urlStatusCode[$url], $type = "string");
            $node1->addNodeAttribute('unique_users', $urlUniqueUsers[$url], $type = "integer");
            $node1->addNodeAttribute('total_users', $urlTotalUsers[$url], $type = "integer");
            $gexf->addNode($node1);
            $node2 = new GexfNode($username);
            $node2->addNodeAttribute("type", 'user', $type = "string");
            $node2->addNodeAttribute('shortlabel', $username, $type = "string");
            $node2->addNodeAttribute('longlabel', $username, $type = "string");
            $node2->addNodeAttribute('unique_urls', $userUniqueUrls[$username], $type = "integer");
            $node2->addNodeAttribute('total_urls', $userTotalUrls[$username], $type = "integer");

            $gexf->addNode($node2);
            $edge_id = $gexf->addEdge($node1, $node2, $frequency);
        }
    }

    $gexf->render();

    $filename = get_filename_for_export("urlUser", '', 'gexf');
    file_put_contents($filename, $gexf->gexfFile);

    // If the current collection is marked for pseudonymization then all the values should be saved after the CSV-loop where we have run our pseudonymization functionality.
    if ($pseudonymized_bool == 1) {
        save_pseudonymized_data($pp, $start_index, $GLOBALS['last_index']);
    }

    echo '<fieldset class="if_parameters">';

    echo '<legend>Your network (GEXF) file</legend>';

    echo '<p><a href="' . filename_to_url($filename) . '">' . $filename . '</a></p>';

    echo '</fieldset>';
    ?>

</body>

</html>