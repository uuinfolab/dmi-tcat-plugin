<?php
require_once __DIR__ . '/common/config.php';
require_once __DIR__ . '/common/functions.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>TCAT :: Mention graph</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="css/main.css" type="text/css" />

    <script type="text/javascript" language="javascript">



    </script>

</head>

<body>

    <h1>TCAT :: Mention graph</h1>

    <?php
    validate_all_variables();
    dataset_must_exist();
    $dbh = pdo_connect();
    pdo_unbuffered($dbh);
    $collation = current_collation();

    $users = array();
    $usersinv = array();
    $edges = array();

    $cur = 0;
    $numresults = 500000;

    //print_r($esc); exit;

    while ($numresults == 500000) {

        $sql = "SELECT m.from_user_name COLLATE $collation as from_user_name, m.to_user COLLATE $collation as to_user FROM " . $esc['mysql']['dataset'] . "_mentions m, " . $esc['mysql']['dataset'] . "_tweets t ";
        $where = "m.tweet_id = t.id AND ";
        $sql .= sqlSubset($where);
        $sql .= " LIMIT " . $cur . "," . $numresults;

        //print $sql."<br>";

        $numresults = 0;

        $rec = $dbh->prepare($sql);
        $rec->execute();

        // Create a boolean variable that gives whether a dataset is marked for pseudonymization or not.
        $pseudonymized_bool = is_pseudonymized($esc['mysql']['dataset']);

        // Fetch the pseudonymization list from the database and save it into the array $pseudo_list2
        $pseudo_list2 = array();
        $pseudo_list2 = fetch_pseudonymized_data();

        // If there exists a pseudonymization list we need the index value from that list since this value will be used as a reference number.
        if ($pseudo_list2 != NULL) {
            $start_index = $last_index = max(array_keys($pseudo_list2));
            $pp = $pseudo_list2;
        } else { // If no pseudonymization lists exist, start from 0.
            $start_index = $last_index = 0;
            $pp = array();
        }

        // The array that contains all the fields in a tweet object that we want to pseudonymize. Created as assocational array for the sake of speed.
        $keyarray = array('location' => 1, 'username' => 1, 'user' => 2, 'id' => 3, 'tweetid' => 4, 'id_string' => 1, 'from_user_id' => 1, 'from_user_name' => 1, 'from_user_realname' => 1, 'user_from_name' => 1, 'user_from_id' => 1, 'user_to_id' => 1, 'user_to_name' => 1, 'to_user' => 1, 'to_user_id' => 1, 'to_user_name' => 1, 'in_reply_to_status_id' => 1, 'in_reply_to_status_id_str' => 1, 'in_reply_to_user_id' => 1, 'in_reply_to_screen_name' => 1, 'quoted_status_id' => 1, 'retweeted_status' => 1, 'retweeted' => 1, 'retweet_id' => 1);


        while ($data = $rec->fetch(PDO::FETCH_ASSOC)) {
            // Use that boolean value to determine whether we should send the fetched dataparts to the function pseudonymized.
            if ($pseudonymized_bool == 1) {
                // Send row-by-row to the pseudonymization functionality together with our index value.
                $last_index  = pseudonymize($data, $pp);
            }


            $numresults++;

            $data["from_user_name"] = strtolower($data["from_user_name"]);
            $data["to_user"] = strtolower($data["to_user"]);

            if (!isset($users[$data["from_user_name"]])) {
                $users[$data["from_user_name"]] = $arrayName = array('id' => count($usersinv), 'notweets' => 1, 'nomentions' => 0);
                $usersinv[] = $data["from_user_name"];
            } else {
                $users[$data["from_user_name"]]["notweets"]++;
            }

            if (!isset($users[$data["to_user"]])) {
                $users[$data["to_user"]] = $arrayName = array('id' => count($usersinv), 'notweets' => 0, 'nomentions' => 1);
                $usersinv[] = $data["to_user"];
            } else {
                $users[$data["to_user"]]["nomentions"]++;
            }

            $to = $users[$data["from_user_name"]]["id"] . "," . $users[$data["to_user"]]["id"];

            if (!isset($edges[$to])) {
                $edges[$to] = 1;
            } else {
                $edges[$to]++;
            }
        }

        $cur = $cur + $numresults;
    }

    //print_r($users);

    $topusers = array();


    foreach ($users as $key => $user) {
        $topusers[$key] = $user["nomentions"];
    }

    arsort($topusers);

    if ($esc["shell"]["topu"] > 0) {
        $topusers = array_slice($topusers, 0, $esc["shell"]["topu"], true);
    }
    //print_r($topusers);


    $content = "nodedef>name VARCHAR,label VARCHAR,no_tweets INT,no_mentions INT\n";
    foreach ($users as $key => $value) {
        if (isset($topusers[$key])) {
            $content .= $value["id"] . "," . $key . "," . $value["notweets"] . "," . $value["nomentions"] . "\n";
        }
    }

    $content .= "edgedef>node1 VARCHAR,node2 VARCHAR,weight DOUBLE,directed BOOLEAN\n";
    foreach ($edges as $key => $value) {
        $tmp = explode(",", $key);
        if (isset($topusers[$usersinv[$tmp[0]]]) && isset($topusers[$usersinv[$tmp[1]]])) {
            $content .= $key . "," . $value . ",true\n";
        }
    }

    //echo $content;

    // add filename for top user filter  "_minDegreeOf".$esc['shell']['minf']
    $filename = get_filename_for_export("mention", "_Top" . $esc['shell']['topu'], "gdf");
    file_put_contents($filename, $content);



    // If the current collection is marked for pseudonymization then all the values should be saved after the CSV-loop where we have run our pseudonymization functionality.
    if ($pseudonymized_bool == 1) {
        save_pseudonymized_data($pp, $start_index, $GLOBALS['last_index']);
    }

    echo '<fieldset class="if_parameters">';

    echo '<legend>Your File</legend>';

    echo '<p><a href="' . filename_to_url($filename) . '">' . $filename . '</a></p>';

    echo '</fieldset>';

    ?>

</body>

</html>