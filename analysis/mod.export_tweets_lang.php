<?php
require_once __DIR__ . '/common/config.php';
require_once __DIR__ . '/common/functions.php';
require_once __DIR__ . '/common/CSV.class.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>TCAT :: Export Tweets language</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="css/main.css" type="text/css" />

    <script type="text/javascript" language="javascript">

    </script>

</head>

<body>

    <h1>TCAT :: Export Tweets language</h1>

    <?php
    validate_all_variables();
    $dbh = pdo_connect();
    pdo_unbuffered($dbh);
    /* @todo, use same export possibilities as mod.export_tweets.php */

    $header = "id,time,created_at,from_user_name,from_user_lang,text,source,location,lat,lng,from_user_tweetcount,from_user_followercount,from_user_friendcount,from_user_realname,to_user_name,in_reply_to_status_id,quoted_status_id,from_user_listed,from_user_utcoffset,from_user_timezone,from_user_description,from_user_url,from_user_verified,filter_leveli,cld_name,cld_code,cld_reliable,cld_bytes,cld_percent";
    if (isset($_GET['includeUrls']) && $_GET['includeUrls'] == 1)
        $header .= ",urls,urls_expanded,urls_followed,domains";
    $header .= "\n";

    $langset = $esc['mysql']['dataset'] . '_lang';

    $sql = "SELECT * FROM " . $esc['mysql']['dataset'] . "_tweets t inner join $langset l on t.id = l.tweet_id ";
    $sql .= sqlSubset();

    $filename = get_filename_for_export("fullExportLang");
    $csv = new CSV($filename, $outputformat);
    $csv->writeheader(explode(',', $header));
    $rec = $dbh->prepare($sql);
    $rec->execute();

    // Create a boolean variable that gives whether a dataset is marked for pseudonymization or not.
    $pseudonymized_bool = is_pseudonymized($esc['mysql']['dataset']);

    // Fetch the pseudonymization list from the database and save it into the array $pseudo_list2
    $pseudo_list2 = array();
    $pseudo_list2 = fetch_pseudonymized_data();

    // If there exists a pseudonymization list we need the index value from that list since this value will be used as a reference number.
    if ($pseudo_list2 != NULL) {
        $start_index = $last_index = max(array_keys($pseudo_list2));
        $pp = $pseudo_list2;
    } else { // If no pseudonymization lists exist, start from 0.
        $start_index = $last_index = 0;
        $pp = array();
    }

    // The array that contains all the fields in a tweet object that we want to pseudonymize. Created as assocational array for the sake of speed.
    $keyarray = array('location' => 1, 'username' => 1, 'user' => 2, 'id' => 3, 'tweetid' => 4, 'id_string' => 1, 'from_user_id' => 1, 'from_user_name' => 1, 'from_user_realname' => 1, 'user_from_name' => 1, 'user_from_id' => 1, 'user_to_id' => 1, 'user_to_name' => 1, 'to_user' => 1, 'to_user_id' => 1, 'to_user_name' => 1, 'in_reply_to_status_id' => 1, 'in_reply_to_status_id_str' => 1, 'in_reply_to_user_id' => 1, 'in_reply_to_screen_name' => 1, 'quoted_status_id' => 1, 'retweeted_status' => 1, 'retweeted' => 1, 'retweet_id' => 1);

    while ($data = $rec->fetch(PDO::FETCH_ASSOC)) {
        // Use that boolean value to determine whether we should send the fetched dataparts to the function pseudonymized.
        if ($pseudonymized_bool == 1) {
            // Send row-by-row to the pseudonymization functionality together with our index value.
            $last_index  = pseudonymize($data, $pp);
        }

        $csv->newrow();
        if (preg_match("/_urls/", $sql))
            $id = $data['tweet_id'];
        else
            $id = $data['id'];
        $csv->addfield($id);
        $csv->addfield(strtotime($data["created_at"]));
        $fields = array('created_at', 'from_user_name', 'from_user_lang', 'text', 'source', 'location', 'geo_lat', 'geo_lng', 'from_user_tweetcount', 'from_user_followercount', 'from_user_friendcount', 'from_user_realname', 'to_user_name', 'in_reply_to_status_id', 'quoted_status_id', 'from_user_listed', 'from_user_utcoffset', 'from_user_timezone', 'from_user_description', 'from_user_url', 'from_user_verified', 'filter_level');
        foreach ($fields as $f) {
            $csv->addfield(isset($data[$f]) ? $data[$f] : '');
        }
        $csv->addfield($data['name']);
        $csv->addfield($data['code']);
        $csv->addfield($data['reliable'] == true ? 1 : 0);
        $csv->addfield($data['bytes']);
        $csv->addfield($data['percent']);
        if (isset($_GET['includeUrls']) && $_GET['includeUrls'] == 1) {
            $urls = $expanded = $followed = $domain = "";
            $sql2 = "SELECT url, url_expanded, url_followed, domain FROM " . $esc['mysql']['dataset'] . "_urls WHERE tweet_id = " . $data['id'];
            $rec2 = $dbh->prepare($sql2);
            $rec2->execute();
            while ($res2 = $rec2->fetch(PDO::FETCH_ASSOC)) {
                $urls = $res2['url'] . " ; ";
                $expanded = $res2['url_expanded'] . " ; ";
                $followed = $res2['url_followed'] . " ; ";
                $domain = $res2['domain'] . " ; ";
            }
            if (!empty($urls)) {
                $urls = substr($urls, 0, -3);
                $expanded = substr($expanded, 0, -3);
                $followed = substr($followed, 0, -3);
                $domain = substr($domain, 0, -3);
            }
            $csv->addfield($urls);
            $csv->addfield($expanded);
            $csv->addfield($followed);
            $csv->addfield($domain);
        }
        $csv->writerow();
    }

    $csv->close();

    // If the current collection is marked for pseudonymization then all the values should be saved after the CSV-loop where we have run our pseudonymization functionality.
    if ($pseudonymized_bool == 1) {
        save_pseudonymized_data($pp, $start_index, $GLOBALS['last_index']);
    }

    echo '<fieldset class="if_parameters">';
    echo '<legend>Your File</legend>';
    echo '<p><a href="' . filename_to_url($filename) . '">' . $filename . '</a></p>';
    echo '</fieldset>';
    ?>

</body>

</html>