<?php
require_once __DIR__ . '/functions.php';
/*
* pseudonymization.php handles all the pseudonymization work.
*/



/**
 * Fetch the data needed for de-pseudonymization from the database, and save it to a multidimensional array.
 *

 */

function fetch_pseudonymized_data()
{
	global $pseudo_list;
	$pseudo_list = array();

	// If not empty, store a copy of the table in $pseudo_list
	$dbh = pdo_connect();
	$sql = "SELECT * FROM tcat_pseudonymized_data;";
	$rec = $dbh->prepare($sql);
	$rec->execute();

	while ($row = $rec->fetch(PDO::FETCH_ASSOC)) {
		//$pseudo_list[$row['pseudo_val']] = array($row['original_data'] => $row['fieldtype']);
		$pseudo_list[$row['original_data']] = array($row['fieldtype'] => $row['pseudo_val']);
	}
	//$pseudo_list=array();
	// Close the database connection before returning.
	$dbh = NULL;
	return $pseudo_list;
}

/**
 * Save the array consisting of the original data and the corresponding pseudonymisation number. By using start value and last value the function makes sure to only add the entries in the array that is not already in the database. Startvalue - Lastvalue = content added.
 *
 * @param array $pseudo_list An array consisting of the original data and pseudonymization ID.
 * @param integer $insert_start_value the value that corresponds to the last entry in the database.
 * @param integer $last_pseudo_index the value that corresponds to the index of the last added value to the array.
 */
function save_pseudonymized_data($pseudo_list, $insert_start_value, $last_pseudo_index)
{
	$dbh = pdo_connect();

	// We only want to add the new pseudovalues and keep the old ones without changing the database.
	$pseudo_list = array_slice($pseudo_list, $insert_start_value, $last_pseudo_index, TRUE);

	foreach ($pseudo_list as $key => $value) {
		try {
			$fieldtype = key($value);
			$pseudo_val = $value[$fieldtype];
			$original_data = $key;

			$sql = "INSERT INTO tcat_pseudonymized_data (pseudo_val, original_data, fieldtype) VALUES (?, ?, ?);";
			$stmt = $dbh->prepare($sql);
			$stmt->execute([$pseudo_val, $original_data, $fieldtype]);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	$dbh = NULL;
}

/**
 * Querys the database with the given dataset to see whether the actual dataset is marked for pseudonymization or not.
 *
 * @param string $dataset A string consisting of the name of the database that we want to check up whether it is flagged for pseudonymization.
 */
function is_pseudonymized($dataset)
{
	$dbh = pdo_connect();
	$sql = "SELECT pseudonymization FROM tcat_query_bins WHERE querybin ='" . $dataset . "';";
	$rec = $dbh->prepare($sql);
	$rec->execute();
	$boolindicator = $rec->fetchcolumn();
	$dbh = NULL;

	return $boolindicator;
}

/**
 * Save the array consisting of the original data and the corresponding pseudonymisation number. By using start value and last value the function makes sure to only add the entries in the array that is not already in the database. Startvalue - Lastvalue = content added.
 * //param &$data a reference to the current row we want to add to a .CSV file.
 * //param &$pseudolist a reference to the current index we are at in the current pseudonymization list.
 */
function pseudonymize(&$data, &$pseudolist)
{

	//Fetch our variables.
	$key_array = $GLOBALS['keyarray'];
	$index = $GLOBALS['last_index'];

	// Walk through every field in the current row that we want to add to the database.
	foreach ($data as $key => &$value) {
		// For every field, check if it is among those that shall be pseudonymized.
		if (array_key_exists($key, $key_array) !== FALSE && $value != NULL) {
			//$exists = array_search($value, $pp);
			// If the value to be pseudonymized already exists, use it.
			if (isset($pseudolist[$value])) {
				$found_value = array_values($pseudolist[$value]);
				$value = $found_value[0];
			} else { // Otherwise expand the pseuonymization table with a new value.
				$index = $index + 1;
				$pseudolist[$value] = array($key => $index);
				$value = $index;
			}
		}
	}
		//If the row contains a textfield, then we run the special pseudonymization for text.
	
			if ((isset($data['text']))) {
	
	// Regular expression that matches words beginning with '@' and ends with whitespace
			$regexp = '/([@][\w_-]+)/';
			$matches = array();
			// Run the regular expression and save the results in the array $matches.
			preg_match_all($regexp, $data['text'], $matches, PREG_PATTERN_ORDER);            

			// Walk through the list of matches. I.e usernames.
			foreach ($matches[0] as $key1 => $value1) {
			// Remove th€ @-sign from the value.
			$value1 = ltrim(($value1), '@');

		// If the value to be pseudonymized already exists, use it.
			if (isset($pseudolist[$value1])) {
				$found_value = array_values($pseudolist[$value1]);
				$mask = $found_value[0];
				$data['text'] = str_replace($value1, ($mask), $data['text']);
			} else { // If the value we want to pseudonymize not exists, add it to our pseudonymization table and do the same thing with the textfield thereafter.
				$index = ($index + 1);
				$pseudolist[$value1] = array('mention in text' => $index);
				$data['text'] = str_replace($value1, ($index), $data['text']);
			}
			}
		}
	return ($index);
}
/**
 * A special variant of the function above that works with more frequiency oriented tables. The function works in lion-part as pseudonymize_field but for a specific field.
 *
 * @param array $results an array consisting of a specific array from a specific function where we want to pseudonymize a specific value.
 */

function pseudonymize_user_name($results)
{
	// Fetch the pseudonymization list from the database and save it into the array $pseudo_list2
	$pseudo_list = array();
	$pseudo_list = fetch_pseudonymized_data();

	// If there exists a pseudonymization list we need the index value from that list since this value will be used as a reference number.
	if ($pseudo_list != NULL) {
		$insert_start_value = $last_pseudo_index = max(array_keys($pseudo_list));
	} else { // If no pseudonymization lists exist, start from 0.
		$insert_start_value = $last_pseudo_index = 0;
	}
	$stored_key = null;
	// Strange array does that we need to fetch an array inside an array.
	foreach ($results as $key => $value) {
		$stored_key = $key;
		$value2 = array();

		// Foreach the array to get to the values that we want to pseudonymise.
		foreach ($value as $key => $val) {



			if (isset($pseudo_list[$key]['to_user'])) {

				$value2[$pseudo_list[$key]['to_user']] = $val;
			} else { // Orherwise, add it fresh.
				$last_pseudo_index = ($last_pseudo_index + 1);
				$newData = array('to_user' => ($last_pseudo_index));
				$pseudo_list[$key] = $newData;
				$value2[($last_pseudo_index)] = $val;
			}
		}
		// Keep an array with pseudonymized values that we return.
		$new_results = array(
			$stored_key => $value2
		);
	}
	// Save this array.
	save_pseudonymized_data($pseudo_list, $insert_start_value, $last_pseudo_index);
	return $new_results;
}
