<?php
require_once('twitteroauth.php');
require_once(__DIR__ . '/../config.php');
require_once(__DIR__ . '/../capture/common/functions.php');

// A function that sends tweets using the existing twitteroauth library.
function sendBeacon($track, $timeSpent, $id)
{
    global $twitter_consumer_key, $twitter_consumer_secret, $twitter_user_token, $twitter_user_secret;

    $connection = new TwitterOAuth($twitter_consumer_key, $twitter_consumer_secret, $twitter_user_token, $twitter_user_secret);

    // The message which can be change after taste and liking.
    $status = "Reading up on " . $track . " since " . $timeSpent . "visit webpage" . $id . ".";

    $post_tweet = $connection->post('statuses/update', ["status" => $status]);

    $connection = null;
}
