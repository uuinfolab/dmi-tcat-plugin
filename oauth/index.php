<?php


// Set Important / Load important
session_start();
require_once('config.php');
require_once('function.php');
require_once('twitteroauth.php');
require_once __DIR__ . '/../common/functions.php';
include_once './oauth_query_manager.php';

// OAuth login check
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    $login_status = "";
    $logged_in = FALSE;
} else {
    $access_token = $_SESSION['access_token'];
    $connection = new TwitterOAuth($tk_oauth_consumer_key, $tk_oauth_consumer_secret, $access_token['oauth_token'], $access_token['oauth_token_secret']);
    $login_info = $connection->get('account/verify_credentials');
    $login_status = "Hi " . $_SESSION['access_token']['screen_name'] . ", are you ready to view your collected tweets?<br><a href='./clearsessions.php'>logout</a>";
    $logged_in = TRUE;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Occurence checker</title>

    <meta charset='<?php echo mb_internal_encoding(); ?>'>
    <style type="text/css">
        body,
        html {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
        }

        #updatewarning {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 8px;
            width: 1024px;
            border: red 1px solid;
        }

        table {
            font-size: 11px;
        }

        th {
            background-color: #ccc;
            padding: 5px;
        }

        th.toppad {
            font-size: 14px;
            text-align: left;
            padding-top: 20px;
            background-color: #fff;
        }

        td {
            background-color: #eee;
            padding: 5px;
        }

        .keywords {
            width: 400px;
        }

        #if_fullpage {
            width: 1000px;
        }

        h1 {
            font-size: 16px;
            margin: 20px 0px 15px 0px;
        }

        #if_title {
            float: left;
        }

        #if_links {
            float: right;
            padding-top: 22px;
            margin-right: -20px;
        }

        .if_toplinks {
            display: inline-block;
            margin-left: 1em;
            font-size: 12px;
            text-decoration: none;
            color: #000;
        }

        .if_toplinks:before {
            content: "» ";
        }

        .if_toplinks:hover {
            text-decoration: underline;
        }

        .if_row {
            padding: 2px;
            position: relative;
            width: 1024px;
            clear: both;
        }

        .if_row_header {
            width: 100px;
            height: 20px;
            float: left;
            padding-top: 5px;
        }

        .if_row_content input {
            width: 320px;
        }

        <?php if ((array_search('track', $captureroles) !== false)) print "#if_row_users { display:none; }";

        ?>th.header {
            background-image: url(../analysis/css/tablesorter/blue/bg.gif);
            cursor: pointer;
            font-weight: bold;
            background-repeat: no-repeat;
            background-position: center left;
            padding-left: 20px;
            border-right: 1px solid #dad9c7;
            margin-left: -1px;
        }

        th.headerSortDown {
            background-image: url(../analysis/css/tablesorter/blue/desc.gif);
            background-color: #3399FF;
        }

        th.headerSortUp {
            background-image: url(../analysis/css/tablesorter/blue/asc.gif);
            background-color: #3399FF;
        }
    </style>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">


</head>

<body>
    <div id="if_fullpage">
        <h1 id="if_title">DMI-TCAT query manager</h1>
        <div id="if_links">
            <a href="https://github.com/digitalmethodsinitiative/dmi-tcat" target="_blank" class="if_toplinks">github</a>
            <a href="https://github.com/digitalmethodsinitiative/dmi-tcat/issues?state=open" target="_blank" class="if_toplinks">issues</a>
            <a href="https://github.com/digitalmethodsinitiative/dmi-tcat/wiki" target="_blank" class="if_toplinks">FAQ</a>
            <a href="./../analysis/index.php" class="if_toplinks">analysis</a>
            <?php
            if (is_admin())
                print '<a href="./../capture/index.php" class="if_toplinks">admin</a>';
            ?>
            <?php
            // If not logged in, show login button.
            if (!(isset($access_token))) {
                echo "<a href='./oauthlogin.php' class='if_toplinks'>Login with your Twitteraccount</a>";
            }
            ?>
        </div>
        <div style="clear:both;"></div>
    </div>
    <div id='main'>
        <?php // message. 
        if (isset($_SESSION['notice'])) { ?>
            <div class='ui-widget'>
                <div class='ui-state-highlight ui-corner-all' style='padding:5px; margin: 5px; width:750px; margin-left:auto; margin-right:auto; text-align:center'><span class='ui-icon ui-icon-info' style='float: left'></span><b><?php echo $_SESSION['notice']; ?></b></div>
            </div>
            <?php
            unset($_SESSION['notice']);
        } ?>
        <div id='login'>
            <?php
            // Login message. 
            echo $login_status; ?>
        </div> <!-- end login div -->
        <?php
        // Fetch all bins from the database
        $querybins = getBins();
        if (isset($access_token)) {
            echo "<h3>Collections where you are represented</h3>";
        } else {
            echo "<h3>Please login with your Twitter credentials to see collections in which you are represented</h3>";
        }
        echo '<table id="thetable">';
        echo '<thead>';
        echo '<tr>';
        echo '<th>querybin</th>';
        echo '<th>active</th>';
        echo '<th>type</th>';
        echo '<th>Pseudonyminized   </th>';
        echo '<th class="keywords">queries</th>';
        echo '<th>no. tweets</th>';
        echo '<th>Periods in which the query bin was active</th>';
        echo '<th>View tweets</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        // Array for saving all bins where our user is represented.
        $containing_bins = array();
        $dbh = pdo_connect();

        // phraselist contains all hashtags or locations that we are currently following.
        $phraseList = array();
        $phrasePeriodsList = array();
        $activePhraselist = array();

        // For every bin in the database.
        foreach ($querybins as $bin) {
            $occurences = 0;
            $sql = "SELECT * FROM " . $bin->name . "_tweets where from_user_id =" . $access_token['user_id'] . ";";
            try {
                $rec = $dbh->prepare($sql);
                $rec->execute();
                // If the actual user is represented in the actual bin.
                if ($row = $rec->fetch(PDO::FETCH_ASSOC)) {
                    // Then count the first occurence and add this bin to the array that contain the bins where our user is represented.
                    $occurences = $occurences + 1;
                    array_push($containing_bins, $bin);
                    // For every record, count the occurence.
                    while ($row = $rec->fetch(PDO::FETCH_ASSOC)) {
                        $occurences = $occurences + 1;
                    }
                }
                $bin->nrOfTweets = $occurences;
            } catch (Exception $e) {
                die("Error upon database connection, make sure you are logged in with your Twitteraccount.");
            }
        }

        // For every bin that contains our user.
        foreach ($containing_bins as $bin) {
            $phraseList = array();
            $phrasePeriodsList = array();
            $activePhraselist = array();

            // Formatting from capture/index.php
            if (strstr($bin->type, "track") !== false || strstr($bin->type, "geotrack") !== false || $bin->type == 'search' || $bin->type == 'import ytk') {
                foreach ($bin->phrases as $phrase) {
                    $phrasePeriodsList[$phrase->id] = array_unique($phrase->periods);
                    $phraseList[$phrase->id] = str_replace("\"", "'", $phrase->phrase);
                    if ($phrase->active) {
                        $activePhraselist[$phrase->id] = str_replace("\"", "'", $phrase->phrase);
                    }
                }
            } elseif (strstr($bin->type, "follow") !== false || strstr($bin->type, "timeline") !== false) {
                foreach ($bin->users as $user) {
                    $phrasePeriodsList[$user->id] = array_unique($user->periods);
                    $phraseList[$user->id] = empty($user->user_name) ? $user->id : $user->user_name;
                    if ($user->active) {
                        $activePhraselist[$user->id] = $user->id;
                    }
                }
            }
            $bin->periods = array_unique($bin->periods);
            sort($bin->periods);
            asort($phraseList);

            $action = ($bin->active == 0) ? "start" : "stop";

            // build our table, one row for every bin.
            echo '<tr>';
            echo '<td valign="top">' . $bin->name . '</td>';
            echo '<td valign="top">' . $bin->active . '</td>';
            echo '<td valign="top">' . $bin->type . '</td>';
            echo '<td valign="top">' . $bin->pseudonymized . '</td>';
            echo '<td class="keywords" valign="top">';
            if ($bin->type != "onepercent") {
                echo '<table width="100%">';
                foreach ($phraseList as $phrase_id => $phrase) {
                    echo "<tr valign='top'><td width='30%'>$phrase</td><td>" . implode("<br>", $phrasePeriodsList[$phrase_id]) . "</td></tr>";
                }
                echo '</table>';
            }
            echo '</td>';
            echo '<td valign="top" align="right">' . number_format($bin->nrOfTweets, 0, ",", ".") . '</td>'; // does not sort well
            echo '<td valign="top">' . implode("<br />", $bin->periods) . '</td>';
            echo '<td valign="top">';
            // If the user wants to see all the tweets in a certain bin he or she gets redirected together with ha $_get.
            echo '<a href="./showTweet.php?bin=' . $bin->name . '">view</a>';
            echo '</td>';
            echo '</tr>';
            echo '</tr>';
        }
        echo '</tbody>';
        echo '</table><br /><br />';
        ?>

    </div>

</body>

</html>