<?php
/*
yourTwapperKeeper - Twitter Archiving Application - http://your.twapperkeeper.com
Copyright (c) 2010 John O'Brien III - http://www.linkedin.com/in/jobrieniii

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// LOOK AT README FOR HOW TO CONFIGURE!!!!!!!!!!

/* Host Information */
$tk_your_url = "https://www.gdpredia.com/oauth/";   // make sure to include the trailing slash
$tk_your_dir = "var/www/dmi-tcat/oauth/";   // make sure to include the trailing slash
$youtwapperkeeper_useragent = "Your TwapperKeeper";   // change to whatever you want!

/* Administrators - Twitter screen name(s) who can administer / start / stop archiving */
$admin_screen_name = array('Engangsgrillen', 'campingplats', 'dvladek', 'matmagnani');

/* Users - Twitter screen names that are allowed to use Your Twapper Keeper site - leaving commented means anyone can use site*/
$auth_screen_name = array('Engangsgrillen', 'campingplats', 'dvladek', 'matmagnani', 'LR', 'digiteracy', 'fundaustek', 'quillis', 'javierruiz', 'kaisirlin', 'esterfrITsch', 'annelils', 'raffaellarovida', 'ethosITU', 'a_b_powell', 's_lehuede', 'IndaMemic', '_edjw');



/* Your Twapper Keeper Twitter Account Information used to query for tweets (this is common for the site) */
$tk_twitter_username = 'campingplats';
$tk_twitter_password = '#Plx@m%Rv=y0';
$tk_oauth_token = '1525831465-0QI35YYN6U0BgJplH61AyDC75CMe5OcXEpo1FuW';
$tk_oauth_token_secret = 'Sl72Do6WECTl5dYNfEkgQHycqMwISlX84qPRo22xPNqtC';

/* Your Twapper Keeper Application Information - setup at http://dev.twitter.com/apps and copy in consumer key and secret */
$tk_oauth_consumer_key = 'xLjOKv9N4pIZYRQY9fJTdc70z';
$tk_oauth_consumer_secret = 'cO4FWgBrllRwz3RKWqcald4XUnnabxizFhNIC8nkmCCDX3XtGB';

/* MySQL Database Connection Information */
define("DB_SERVER", "127.0.0.1");   // change to your hostname
define("DB_USER", "virtmtk");      // change to your db username
define("DB_PASS", "v1rt-EU2016");                  // change to your db password
define("DB_NAME", "virtMyTWK");                               // change to your db name

/* Don't mess with this unless you want to get your hands dirty */
$yourtwapperkeeper_version = "version 0.6.0";
$archive_process_array = array('yourtwapperkeeper_crawl.php', 'yourtwapperkeeper_stream.php', 'yourtwapperkeeper_stream_process.php');
$twitter_api_sleep_min = 11;
$stream_process_stack_size = 500;
$php_mem_limit = "512M";
ini_set("memory_limit", $php_mem_limit);
