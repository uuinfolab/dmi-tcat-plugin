<?php
include_once __DIR__ . '/../config.php';
include_once __DIR__ . '/../common/constants.php';
include_once __DIR__ . '/../common/functions.php';

// A function from the capture/query_manager.php. The function is here because of access authority reasons. 
// The function return all the bins from the database.
function getBins()
{

    $dbh = pdo_connect();

    // select query bins
    // select phrases
    // select users

    $sql = "SELECT b.id, b.querybin, b.type, b.comments, b.active, b.pseudonymization, b.beacon, period.starttime AS bin_starttime, period.endtime AS bin_endtime FROM tcat_query_bins b, tcat_query_bins_periods period WHERE b.id = period.querybin_id AND b.access != " . TCAT_QUERYBIN_ACCESS_INVISIBLE . " GROUP BY b.id";

    $rec = $dbh->prepare($sql);
    $rec->execute();
    $bin_results = $rec->fetchAll();
    $querybins = array();
    foreach ($bin_results as $data) {
        if (!isset($querybins[$data['id']])) {
            $bin = new stdClass();
            $bin->periods = array();
            $bin->phrases = array();
            $bin->users = array();
            $bin->id = $data['id'];
            $bin->name = $data['querybin'];
            $bin->type = $data['type'];
            $bin->active = $data['active'];
            $bin->comments = $data['comments'];
            $bin->pseudonymized = $data['pseudonymization'];
            $bin->beacon = $data['beacon'];
        } else {
            $bin = $querybins[$data['id']];
        }
        $bin->periods[] = $data['bin_starttime'] . " - " . str_replace("0000-00-00 00:00:00", "now", $data['bin_endtime']);

        if ($bin->type == "track" || $bin->type == "geotrack") {
            $sql = "SELECT p.id AS phrase_id, p.phrase, bp.starttime AS phrase_starttime, bp.endtime AS phrase_endtime FROM tcat_query_phrases p, tcat_query_bins_phrases bp WHERE p.id = bp.phrase_id AND bp.querybin_id = " . $bin->id;

            $rec = $dbh->prepare($sql);
            $rec->execute();
            $phrase_results = $rec->fetchAll();
            foreach ($phrase_results as $result) {
                if (!isset($bin->phrases[$result['phrase_id']])) {
                    $phrase = new stdClass();
                    $phrase->id = $result['phrase_id'];
                    $phrase->phrase = $result['phrase'];
                    $phrase->periods = array();
                    $phrase->active = false;
                } else {
                    $phrase = $bin->phrases[$result['phrase_id']];
                }
                if ($result["phrase_endtime"] == "0000-00-00 00:00:00")
                    $phrase->active = true;

                $phrase->periods[] = $result['phrase_starttime'] . " - " . str_replace("0000-00-00 00:00:00", "now", $result["phrase_endtime"]);
                $bin->phrases[$result['phrase_id']] = $phrase;
            }
        } elseif ($bin->type == "follow" || $bin->type == "timeline") {
            $sql = "SELECT u.id AS user_id, u.user_name, bu.starttime AS user_starttime, bu.endtime AS user_endtime FROM tcat_query_users u, tcat_query_bins_users bu WHERE u.id = bu.user_id AND bu.querybin_id = " . $bin->id;
            $rec = $dbh->prepare($sql);
            $rec->execute();
            $user_results = $rec->fetchAll();
            foreach ($user_results as $result) {
                if (!isset($bin->users[$result['user_id']])) {
                    $user = new stdClass();
                    $user->id = $result['user_id'];
                    $user->user_name = $result['user_name'];
                    $user->periods = array();
                    $user->active = false;
                } else {
                    $user = $bin->users[$result['user_id']];
                }
                if ($result["user_endtime"] == "0000-00-00 00:00:00")
                    $user->active = true;

                $user->periods[] = $result['user_starttime'] . " - " . str_replace("0000-00-00 00:00:00", "now", $result["user_endtime"]);
                $bin->users[$result['user_id']] = $user;
            }
        }
        $querybins[$bin->id] = $bin;
    }



    // get nr of tweets per bin
    foreach ($querybins as $bin) {
        $querybins[$bin->id]->nrOfTweets = 0;
        $sql = "SELECT count(id) AS count FROM " . $bin->name . "_tweets";
        $res = $dbh->prepare($sql);
        if ($res->execute() && $res->rowCount()) {
            $result = $res->fetch();
            $querybins[$bin->id]->nrOfTweets = $result['count'];
        }
    }
    $dbh = false;
    return $querybins;
}
