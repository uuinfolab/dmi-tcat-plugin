<?php


// Set Important / Load important
session_start();
require_once('config.php');
require_once('function.php');
require_once('twitteroauth.php');

include_once './oauth_query_manager.php';
require_once __DIR__ . '/../common/functions.php';


$querybins = getBins();



// OAuth login check
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    $login_status = "<a href='./oauth/oauthlogin.php' ><img src='./resources/lighter.png'/></a>";
    $logged_in = FALSE;
} else {
    $access_token = $_SESSION['access_token'];
    $connection = new TwitterOAuth($tk_oauth_consumer_key, $tk_oauth_consumer_secret, $access_token['oauth_token'], $access_token['oauth_token_secret']);
    $login_info = $connection->get('account/verify_credentials');
    $login_status = "Hi " . $_SESSION['access_token']['screen_name'] . ", below are your tweet(s) from the " . htmlspecialchars($_GET['bin']) . " collection.<br><a href='./clearsessions.php'>logout</a>";
    $logged_in = TRUE;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>Your tweets</title>
    <meta charset='<?php echo mb_internal_encoding(); ?>'>
    <style type="text/css">
        body,
        html {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
        }

        #updatewarning {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 8px;
            width: 1024px;
            border: red 1px solid;
        }

        table {
            font-size: 11px;
        }

        th {
            background-color: #ccc;
            padding: 5px;
        }

        th.toppad {
            font-size: 14px;
            text-align: left;
            padding-top: 20px;
            background-color: #fff;
        }

        td {
            background-color: #eee;
            padding: 5px;
        }

        .keywords {
            width: 400px;
        }

        #if_fullpage {
            width: 1000px;
        }

        h1 {
            font-size: 16px;
            margin: 20px 0px 15px 0px;
        }

        #if_title {
            float: left;
        }

        #if_links {
            float: right;
            padding-top: 22px;
            margin-right: -20px;
        }

        .if_toplinks {
            display: inline-block;
            margin-left: 1em;
            font-size: 12px;
            text-decoration: none;
            color: #000;
        }

        .if_toplinks:before {
            content: "» ";
        }

        .if_toplinks:hover {
            text-decoration: underline;
        }

        .if_row {
            padding: 2px;
            position: relative;
            width: 1024px;
            clear: both;
        }

        .if_row_header {
            width: 100px;
            height: 20px;
            float: left;
            padding-top: 5px;
        }

        .if_row_content input {
            width: 320px;
        }

        <?php if ((array_search('track', $captureroles) !== false)) print "#if_row_users { display:none; }";

        ?>th.header {
            background-image: url(../analysis/css/tablesorter/blue/bg.gif);
            cursor: pointer;
            font-weight: bold;
            background-repeat: no-repeat;
            background-position: center left;
            padding-left: 20px;
            border-right: 1px solid #dad9c7;
            margin-left: -1px;
        }

        th.headerSortDown {
            background-image: url(../analysis/css/tablesorter/blue/desc.gif);
            background-color: #3399FF;
        }

        th.headerSortUp {
            background-image: url(../analysis/css/tablesorter/blue/asc.gif);
            background-color: #3399FF;
        }
    </style>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">


</head>

<body>
    <div id="if_fullpage">
        <h1 id="if_title">DMI-TCAT query manager</h1>
        <div id="if_links">
            <a href="https://github.com/digitalmethodsinitiative/dmi-tcat" target="_blank" class="if_toplinks">github</a>
            <a href="https://github.com/digitalmethodsinitiative/dmi-tcat/issues?state=open" target="_blank" class="if_toplinks">issues</a>
            <a href="https://github.com/digitalmethodsinitiative/dmi-tcat/wiki" target="_blank" class="if_toplinks">FAQ</a>
            <a href="../analysis/index.php" class="if_toplinks">analysis</a>
            <?php
            if (is_admin())
                print '<a href="./../capture/index.php" class="if_toplinks">admin</a>';
            ?>
            <?php
            // Making sure user are logged in.
            if (!(isset($access_token))) {
                echo "<a href='./oauthlogin.php' class='if_toplinks'>Login with your Twitteraccount</a>";
            }
            ?>
        </div>
        <div style="clear:both;"></div>
    </div>
    <div id='main'>
        <?php
        echo "<h3>Your tweet(s) from the " . htmlspecialchars($_GET['bin']) . " collection.</h3>";
        echo '<table id="thetable">';
        echo '<thead>';
        echo '<tr>';
        echo '<th>date sent</th>';
        echo '<th>text</th>';
        echo '</tr>';
        echo '</thead>';
        echo '<tbody>';

        $dbh = pdo_connect();
        try {
            // For every tweet in the actual bin that matches the user id, print it as a row in the table.
            $sql = "SELECT created_at, text FROM " . htmlspecialchars($_GET['bin']) . "_tweets where from_user_id =" . $access_token['user_id'] . ";";
            $rec = $dbh->prepare($sql);
            $rec->execute();
            while ($row = $rec->fetch(PDO::FETCH_ASSOC)) {
                echo "<td>" . $row['created_at'] . "</td>";
                echo "<td>" . $row['text'] . "</td>";
                echo '</tr>';
            }
        } catch (Exception $e) {
            die("Error upon database connection in OAuth.");
        }
        echo '</tr>';
        echo '</tbody>';
        echo '</table><br /><br />';
        // Link back to the page before.
        echo '<a href="./index.php">Go back to view all occurences of your tweets.</a>';
        ?>
    </div>

</body>

</html>