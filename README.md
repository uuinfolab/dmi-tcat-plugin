The Digital Methods Initiative Twitter Capture and Analysis Toolset (DMI-TCAT) allows one to retrieve and collect tweets from Twitter and to analyze them in various ways.

This repository, created as part of the Virt-EU project, adds basic functionality to make it easier to perform GDPR compliant data collections.

Please check https://github.com/digitalmethodsinitiative/dmi-tcat/wiki for further information and installation instructions.
